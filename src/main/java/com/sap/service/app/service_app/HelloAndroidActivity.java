package com.sap.service.app.service_app;

import java.util.List;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.sap.frontedn.model.DaoMaster;
import com.sap.frontedn.model.DaoMaster.DevOpenHelper;
import com.sap.frontedn.model.DaoSession;
import com.sap.frontedn.model.ServiceDao;
import com.sap.service.cloud.factory.CloudFactoryService;
import com.sap.service.model.Service;

public class HelloAndroidActivity extends Activity {
	private Button createButton;
	private Button readButton;
	private Button updateButton;
	private Button deleteButton;
	private Service service;
	private Long ID;
	private Long localID;

	private DaoMaster daoMaster;
	private DaoSession daoSession;
	private ServiceDao serviceDao;
	private SQLiteDatabase db;

	/**
	 * Called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 *            If the activity is being re-initialized after previously being shut down then this Bundle contains the data it most recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		createButton = (Button) findViewById(R.id.btnCreate);
		readButton = (Button) findViewById(R.id.btnRead);
		updateButton = (Button) findViewById(R.id.btnUpdate);
		deleteButton = (Button) findViewById(R.id.btnDelete);

		DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "service-db", null);
		db = helper.getWritableDatabase();
		daoMaster = new DaoMaster(db);
		daoSession = daoMaster.newSession();
		serviceDao = daoSession.getServiceDao();

		createButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				setUp();
				new create().execute(service);
			}
		});

		readButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				new read().execute(ID);
			}
		});

		updateButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				setUp();
				service.setId(ID);
				service.setName("fix gyzer");
				new update().execute(service);
			}
		});

		deleteButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				new delete().execute(ID);
			}
		});
	}

	// *********************************************************************************************************

	public class readAll extends AsyncTask<Void, Void, List<Service>> {
		@Override
		protected List<Service> doInBackground(Void... params) {

			List<Service> services = CloudFactoryService.MOCKUP_SERVICE.readAll();

			if (services != null && services.size() > 0l) {
				return services;
			}
			return null;
		}

		@Override
		protected void onPostExecute(List<Service> result) {
			Log.i("All Services **********************************", ": " + result);

		}

	}

	public class create extends AsyncTask<Service, Void, Long> {
		@Override
		protected Long doInBackground(Service... services) {

			Long id = CloudFactoryService.MOCKUP_SERVICE.create(services[0]);
			// Long id = saveLocalService();

			if (id != null && id > 0l) {
				return id;
			}
			return -1l;
		}

		@Override
		protected void onPostExecute(Long result) {
			ID = result;
			// localID = result;
			Log.i("Created Service ID **********************************", ": " + result);
			Toast toast = Toast.makeText(getApplicationContext(), "Service Added ID: " + result, Toast.LENGTH_LONG);
			toast.show();

		}

	}

	public class update extends AsyncTask<Service, Void, Long> {
		@Override
		protected Long doInBackground(Service... services) {
			service.setName("Gyzer fix");
			Long id = CloudFactoryService.MOCKUP_SERVICE.update(services[0]);

			if (id != null && id > 0l) {
				ID = id;
				return id;
			}
			return -1l;
		}

		@Override
		protected void onPostExecute(Long result) {
			Log.i("Updated Service ID **********************************", ": " + result);

			Toast toast = Toast.makeText(getApplicationContext(), "Updated Service ID: " + result, Toast.LENGTH_LONG);
			toast.show();

		}

	}

	public class delete extends AsyncTask<Long, Void, Service> {
		@Override
		protected Service doInBackground(Long... ids) {

			Service deletedService = CloudFactoryService.MOCKUP_SERVICE.delete(ids[0]);

			if (deletedService != null) {
				return deletedService;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Service result) {
			Log.i("Service deleted *********************************", ": " + result.getName());
			Toast toast = Toast.makeText(getApplicationContext(), "Service deleted: " + result.getName(), Toast.LENGTH_LONG);
			toast.show();

		}

	}

	public class read extends AsyncTask<Long, Void, Service> {
		@Override
		protected Service doInBackground(Long... ids) {

			Service service = CloudFactoryService.MOCKUP_SERVICE.read(ids[0]);
			// com.sap.frontedn.model.Service service = readLocalService();

			if (service != null) {
				return service;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Service result) {
			Log.i("Service ***********************************", ": " + result.getName());
			Toast toast = Toast.makeText(getApplicationContext(), "Returned Service: " + result.getName(), Toast.LENGTH_LONG);
			toast.show();
		}

	}

	private long saveLocalService() {
		com.sap.frontedn.model.Service serviceDB = new com.sap.frontedn.model.Service();
		serviceDB.setDescription("DB to save");
		serviceDB.setName("test name");
		long id = serviceDao.insert(serviceDB);
		return id;
	}

	private com.sap.frontedn.model.Service readLocalService() {
		com.sap.frontedn.model.Service serviceDB = new com.sap.frontedn.model.Service();

		serviceDB = serviceDao.load(localID);
		return serviceDB;
	}

	private void setUp() {

		service = new Service();
		service.setDescription("fixing pipes");
		service.setName("Pipe Fix");
		service.setEstimatedDuration(45d);
		service.setSetFee(200d);
	}
}
